package com.sun.leo.appwidgetsample

import android.appwidget.AppWidgetManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.app_widget_settings_activity.*

class AppWidgetSettingsActivity : AppCompatActivity() {

    companion object {
        private const val PREFS_NAME = "com.sun.leo.appwidgetsample.NewAppWidget"
        private const val PREF_PREFIX_KEY = "prefix_"

        // Read the prefix from the SharedPreferences object for this widget.
        // If there is no preference saved, get the default from a resource
        fun loadTitlePref(context: Context, appWidgetId: Int): String? {
            val prefs = context.getSharedPreferences(PREFS_NAME, 0)
            val prefix = prefs.getString(PREF_PREFIX_KEY + appWidgetId, null)
            return prefix ?: context.getString(R.string.appwidget_text)
        }

        // Write the prefix to the SharedPreferences object for this widget
        fun saveTitlePref(context: Context, appWidgetId: Int, text: String?) {
            val prefs = context.getSharedPreferences(PREFS_NAME, 0).edit()
            prefs.putString(PREF_PREFIX_KEY + appWidgetId, text)
            prefs.apply()
        }
    }

    var mAppWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID

    override fun onCreate(icicle: Bundle?) {
        super.onCreate(icicle)

        // Set the result to CANCELED.  This will cause the widget host to cancel
        // out of the widget placement if they press the back button.
        setResult(RESULT_CANCELED)

        // Set the view layout resource to use.
        setContentView(R.layout.app_widget_settings_activity)

        // Find the widget id from the intent.
        val extras = intent.extras
        if (extras != null) {
            mAppWidgetId = extras.getInt(
                AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID
            )
        }

        // If they gave us an intent without the widget id, just bail.
        if (mAppWidgetId == AppWidgetManager.INVALID_APPWIDGET_ID) {
            finish()
        }
        appwidget_prefix.setText(loadTitlePref(this, mAppWidgetId))

        // Bind the action for the save button.
        save_button.setOnClickListener {
            val context: Context = this@AppWidgetSettingsActivity

            // When the button is clicked, save the string in our prefs and return that they
            // clicked OK.
            val titlePrefix = appwidget_prefix.text.toString()
            saveTitlePref(context, mAppWidgetId, titlePrefix)

            // Push widget update to surface with newly set prefix
            val appWidgetManager = AppWidgetManager.getInstance(context)
            NewAppWidget.updateAppWidget(context, appWidgetManager, mAppWidgetId)

            // Make sure we pass back the original appWidgetId
            val resultValue = Intent()
            resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, mAppWidgetId)
            setResult(RESULT_OK, resultValue)
            finish()
        }
    }

}