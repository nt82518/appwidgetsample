package com.sun.leo.appwidgetsample

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.RemoteViews

/**
 * Implementation of App Widget functionality.
 */
class NewAppWidget : AppWidgetProvider() {

    companion object {
        private const val TAG = "NewAppWidget"

        fun updateAppWidget(
            context: Context, appWidgetManager: AppWidgetManager,
            appWidgetId: Int
        ) {
            val widgetText = AppWidgetSettingsActivity.loadTitlePref(context, appWidgetId)
                ?: context.getString(R.string.appwidget_text)

            // Construct the RemoteViews object.  It takes the package name (in our case, it's our
            // package, but it needs this because on the other side it's the widget host inflating
            // the layout from our package).
            val views = RemoteViews(context.packageName, R.layout.new_app_widget)
            views.setTextViewText(R.id.appwidget_text, widgetText)

            views.setOnClickPendingIntent(
                R.id.appwidget_layout,
                getPendingIntent(context, appWidgetId)
            )

            // Tell the widget manager
            appWidgetManager.updateAppWidget(appWidgetId, views)
        }

        private fun getPendingIntent(context: Context, appWidgetId: Int): PendingIntent {
            val intent = Intent(context, AppWidgetSettingsActivity::class.java)
            intent.action = "android.appwidget.action.APPWIDGET_CONFIGURE"
            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId)
            return PendingIntent.getActivity(context, appWidgetId, intent, 0)
        }
    }

    override fun onUpdate(
        context: Context,
        appWidgetManager: AppWidgetManager,
        appWidgetIds: IntArray
    ) {
        Log.e(TAG, "onUpdate")

        // There may be multiple widgets active, so update all of them
        for (appWidgetId in appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId)
        }
    }

    override fun onEnabled(context: Context?) {
        Log.e(TAG, "onEnabled")
    }

    override fun onDisabled(context: Context?) {
        Log.e(TAG, "onDisabled")
    }

    override fun onDeleted(context: Context?, appWidgetIds: IntArray?) {
        Log.e(TAG, "onDeleted")
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        Log.e(TAG, "onReceive")
        super.onReceive(context, intent)
    }
}